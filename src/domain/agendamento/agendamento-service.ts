import { AgendamentoDAO } from './agendamento-dao';
import { Storage } from '@ionic/storage';
import { Agendamento } from './agendamento';
import { Http } from '@angular/http';
import { Injectable } from "@angular/core";

@Injectable()
export class AgendamentoService {

    constructor(private _http: Http, private _dao: AgendamentoDAO) {}

    agenda(agendamento: Agendamento) {
        let api = `https://aluracar.herokuapp.com/salvarpedido?carro=${agendamento.carro.nome}&preco=${agendamento.valor}&nome=${agendamento.nome}&endereco=${agendamento.endereco}&email=${agendamento.email}&dataAgendamento=${agendamento.data}`;
        
        return this._dao.ehAgendamentoDuplicado(agendamento)
            .then(existe => {
                if(existe) throw new Error('Agendamento já existente!') 
                return this._http
                    .get(api)
                    .toPromise()
                    .then(() =>  agendamento.confirmado = true, err => console.log(err))
                    .then(() => this._dao.salva(agendamento))
                    .then(() => agendamento.confirmado); 
            })  
    }
}